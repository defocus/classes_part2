﻿using System;

namespace Task1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter sum UAH would you like to convert: ");
            double uah1 = double.Parse(Console.ReadLine());

            Converter convert = new(uah: uah1);

            Console.WriteLine("Choose currancy to convert:");
            Console.WriteLine("> 1 -  USD");
            Console.WriteLine("> 2 -  EUR");
            string operation = Console.ReadLine();

            Console.WriteLine(
                "Sum convertation: " +
                operation switch
                {
                    "1" => convert.UAHtoUSD,
                    "2" => convert.UAHtoEUR,
                    _ => null
                }
            );

        }
    }
}
