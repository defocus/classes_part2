﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    internal class Converter
    {
        double _usd;
        double _eur;
        double _uah;

        public double USD => _usd;
        public double EUR => _eur;
        public double UAh => _uah;

        public double USDtoUAH => UsdToUah(_usd);
        public double EURtoUAH => EurToUah(_eur);
        public double UAHtoUSD => UahToUsd(_uah);
        public double UAHtoEUR => UahToEur(_uah);


        public Converter(double usd = 0, double eur = 0, double uah = 0)
        {
            _usd = usd;
            _eur = eur;
            _uah = uah;
        }

        double UsdToUah (double usd) => 37.0 * usd;
        double UahToUsd (double uah) => uah/38.0;
        double EurToUah (double eur) => 38.0 * eur;
        double UahToEur (double uah) => uah/40.0;
    }
}
