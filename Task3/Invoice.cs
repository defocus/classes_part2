﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    internal class Invoice
    {
        readonly double _account;
        readonly string _customer;
        readonly string _provider;
        
        public double Account => _account;
        public string Customer => _customer;
        public string Provider => _provider;

        public Invoice(double account, string customer, string provider)
        {
            _account = account;
            _customer = customer;
            _provider = provider;
        }
        
        public static double Cheque(int[] price)
        {
            double sum = 0;
            
            for (int i = 0; i < price.Length; i++)
            {
                sum += price[i];
            }

            Console.WriteLine($"Account withuot VAT: {sum}");            

            sum += sum * 0.2;

            Console.WriteLine($"Account with VAT: {sum}");

            return sum;
        }
    }
}
