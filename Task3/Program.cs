﻿using System;

namespace Task3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string[] article = { "product1", "product2", "product3", "product4", "product5" };
            
            int[] price = { 100, 150, 200, 250, 300 };

            int[] quantity = new int[5];

            int j = 0;
            
            string a;

            do
            {
                Console.WriteLine("Choose product:");

                for (int i = 0; i < price.Length; i++)
                {
                    Console.WriteLine($"> {i}:\t{article[i]} - {price[i]}");
                }

                int choose = int.Parse(Console.ReadLine());

                Console.Write("Enter quantity: ");

                int temp = int.Parse(Console.ReadLine());

                quantity[j] = price[choose] * temp;

                Console.Write("Do you want to continue shopping?(y/n): ");

                a = Console.ReadLine();

                j++;
            } 
            while (a == "y" && j <= quantity.Length);

            Invoice invoice = new Invoice(
                Invoice.Cheque(quantity),
                "Customer",
                "AlexLev"
                );

            Console.WriteLine(
                $"\n{invoice.Customer}\n" +
                $"\t{invoice.Account}\n" +
                $"\t{invoice.Provider}"
                );
        }
    }
}
