﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task2.Product;

namespace Task2.Factories
{
    internal class EmployeFactory : EmployesFactory
    {
        readonly string _name;
        readonly string _surname;
        readonly int _expirience;

        public EmployeFactory(string[] fullName, int expirience)
        {
            _name = fullName[0];
            _surname = fullName[1];
            _expirience = expirience;
        }

        public override IEmployes GetEmployes()
        {
            Employe employe = new Employe(_name, _surname, _expirience);
            
            //employe.Salary();
            
            return employe;            
        }
    }
}
