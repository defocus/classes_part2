﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task2.Product;

namespace Task2.Factories
{
    internal class DirectoratFactory : EmployesFactory
    {
        readonly string _name;
        readonly string _surname;
        readonly int _expirience;

        public DirectoratFactory(string[] fullName, int expirience)
        {
            _name = fullName[0];
            _surname = fullName[1];
            _expirience = expirience;
        }

        public override IEmployes GetEmployes()
        {
            Directorat directorat = new Directorat(_name, _surname, _expirience);

            return directorat;
        }
    }
}
