﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2.Product
{
    internal class Directorat : IEmployes
    {
        readonly string _name;
        readonly string _surname;
        readonly int _expirience;

        public Directorat(string name, string surname, int expirience)
        {
            _name = name;
            _surname = surname;
            _expirience = expirience;
        }

        double Coefficient(int expirience)
        {
            if (expirience >= 0 && expirience < 1) return 0;

            else if (expirience >= 1 && expirience < 3) return 0.4;

            else if (expirience >= 3 && expirience < 5) return 0.6;

            else if (expirience >= 5 && expirience < 10) return 0.8;

            else if (expirience >= 10) return 1.0;

            else return 0.0;
        }

        public string Name => _name;
        public string Surname => _surname;
        public int Expirience => _expirience;

        public decimal Salary() => (decimal)(((Coefficient(_expirience) * 1200.0) + 1200.0) * 0.8);
    }
}
