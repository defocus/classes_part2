﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2.Product
{
    internal interface IEmployes
    {
        string Name { get; }
        string Surname { get; }
        int Expirience { get; }

        decimal Salary();        
    }
}
