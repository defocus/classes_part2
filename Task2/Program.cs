﻿using System;
using Task2.Factories;
using Task2.Product;

namespace Task2
{
    internal class Program
    {
        static string[] fullName = new string[2];
        
        static int expirience;

        static void Main(string[] args)
        {
            Console.Write("Enter Name employe: ");
            fullName[0] = Console.ReadLine();

            Console.Write("Enter Surname employe: ");
            fullName[1] = Console.ReadLine();

            Console.Write("Enter expiriance: ");
            expirience = int.Parse(Console.ReadLine());

            Console.WriteLine("Choose position in the company:");
            Console.WriteLine("> 1 - Employe");
            Console.WriteLine("> 2 - Administration");
            Console.WriteLine("> 3 - Directorat");

            string position = Console.ReadLine();

            EmployesFactory factory = GetFactory(position);

            IEmployes employe = factory.GetEmployes();

            Console.WriteLine("\nEmploye you just created\n");

            Console.WriteLine(
                $"\tName:\t\t{employe.Name}\n" +
                $"\tSurname:\t{employe.Surname}\n" +
                $"\tExpirience:\t{employe.Expirience}\n" +
                $"\tSalary:\t\t{employe.Salary()}\n"
                );

            Console.ReadKey();
        }

        static EmployesFactory GetFactory(string position) =>
            position switch
            {
                "1" => new EmployeFactory(fullName, expirience),
                "2" => new AdministrationFactory(fullName, expirience),
                "3" => new DirectoratFactory(fullName, expirience),
                _ => null
            };
        
    }
}
