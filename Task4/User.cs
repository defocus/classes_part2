﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4
{
    internal class User
    {
        string login;
        string name;
        string surname;
        int age;
        readonly DateTime _date;

        public string Login { get => login; set => login = value; }
        public string Name { get => name; set => name = value; } 
        public string Surname { get=> surname; set => surname = value; }
        public int Age { get => age; set => age = value; }
        public DateTime Date => _date;

        public User(DateTime data)
        {
            _date = data;
        }
    }
}
