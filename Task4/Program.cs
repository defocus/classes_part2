﻿using System;

namespace Task4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("How many would you like add Users?");
            int index = int.Parse(Console.ReadLine());

            User[] users = new User[index];

            for (int i = 0; i < users.Length; i++)
            {
                users[i] = new User(DateTime.Now);
                
                Console.Write("\nEnter login: ");
                users[i].Login = Console.ReadLine();

                Console.Write("Enter name: ");
                users[i].Name = Console.ReadLine();

                Console.Write("Enter surname: ");
                users[i].Surname = Console.ReadLine();
                
                Console.Write("Enter age: ");
                users[i].Age = int.Parse(Console.ReadLine());
            }

            Console.WriteLine("----------------------------------------------------------\n");

            for (int i = 0; i < users.Length; i++)
            {
                Console.WriteLine(
                    $"User login: {users[i].Login}\n" +
                    $"\t Name: \t\t\t{users[i].Name}\n" +
                    $"\t Surname: \t\t{users[i].Surname}\n" +
                    $"\t Age: \t\t\t{users[i].Age}\n" +
                    $"\t Date registration: \t{users[i].Date}\n"
                    );
            }
        }
    }
}
